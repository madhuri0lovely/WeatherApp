import { Component, OnInit } from '@angular/core';
import { ForecastService } from '../forecast.service';
import { weatherData } from '../shared/model/weatherData.model';
import { WeatherDetails } from '../shared/model/weatherDetails.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-future-weather',
  templateUrl: './future-weather.component.html',
  styleUrls: ['./future-weather.component.css'],
})
export class FutureComponent implements OnInit {
  weatherData: weatherData[] = [];
  errorMessage: HttpErrorResponse;

  constructor(private forecastService: ForecastService) {}

  ngOnInit(): void {
    // calling the service and getting the data
    this.forecastService.getWeatherForecast().subscribe({
      next: (data) => {
        this.futureForecast(data);
      },

      error: (data) => {
        this.errorMessage = data;
      },
    });
    // recieving from today weather component through service
    this.forecastService.onSearchClick.subscribe({
      next: (value) => {
        this.weatherData = [];
        this.futureForecast(value);
      },
    });

    // when api fails
    this.forecastService.hasError.subscribe({
      next: (errorVal) => {
        this.errorMessage = errorVal;
      },
    });
  }

  futureForecast(data: WeatherDetails) {
    for (let i = 1; i < data.list.length; i = i + 8) {
      this.weatherData.push(data.list[i]);
    }
  }
}
