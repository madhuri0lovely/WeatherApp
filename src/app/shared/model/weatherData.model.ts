import { clouds } from "./clouds.model";
import { main } from "./main.model";
import { sys } from "./sys.model";
import { weather } from "./weather.model";
import { wind } from "./wind.model";

export class weatherData {
    public dt: number;
    public main: main;
    public weather: weather[];
    public clouds: clouds;
    public wind: wind;
    public visibility: number;
    public pop: number;
    public sys: sys;
    public dt_txt: string;
}