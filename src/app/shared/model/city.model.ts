import { coord } from "./coord.model";

export class city {
    public id: number;
    public name: string;
    public coord: coord;
    public country: string;
    public population: number;
    public timezone: number;
    public sunrise: number;
    public sunset: number;
}