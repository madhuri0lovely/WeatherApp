export class weather {
    public id: number;
    public main: string;
    public description: string;
    public icon: string;
}