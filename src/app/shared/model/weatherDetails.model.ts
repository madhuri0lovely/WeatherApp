import { city } from "./city.model";
import { weatherData } from "./weatherData.model";

export class WeatherDetails {
    public cod: string;
    public message: string;
    public cnt: number;
    public list: weatherData[];
    public city: city;
}