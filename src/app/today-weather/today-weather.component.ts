import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ForecastService } from '../forecast.service';
import { WeatherDetails } from '../shared/model/weatherDetails.model';
import { weatherData } from '../shared/model/weatherData.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-today-weather',
  templateUrl: './today-weather.component.html',
  styleUrls: ['./today-weather.component.css'],
})
export class TodayComponent implements OnInit {
  weatherNow: weatherData;
  currentTime = new Date(); //return the date and time of current device/country
  city = '';
  country = '';
  errorMessage: HttpErrorResponse;

  @ViewChild('citysearchform', { static: false }) searchForm: NgForm;

  constructor(private forecastService: ForecastService) {}

  ngOnInit(): void {
    // calling the service and getting the data
    this.forecastService.getWeatherForecast().subscribe({
      next: (data) => {
        this.getTodayForecast(data);
      },
      error: (error) => {
        this.errorMessage = error;
      },
    });
  }

  getTodayForecast(today: WeatherDetails) {
    this.city = today.city.name;
    this.country = today.city.country;
    for (const forecast of today.list.slice(1, 7)) {
      this.weatherNow = forecast;
    }
  }

  // on click of search button
  onSearch() {
    const searchCityOrCountry = this.searchForm.value.input;
    // only when there is some input value present
    if (searchCityOrCountry != '') {
      this.forecastService.getWeatherByCityName(searchCityOrCountry).subscribe({
        next: (data) => {
          this.getSearchCityForecast(data);
        },
        error: (error) => {
          this.errorMessage = error;
          this.forecastService.hasError.next(error);
        },
      });
    }
  }

  getSearchCityForecast(response: WeatherDetails) {
    // sending to future weather component through service
    this.forecastService.onSearchClick.next(response);
    this.city = response.city.name;
    this.country = response.city.country;
    for (const searchForecast of response.list) {
      this.weatherNow = searchForecast;
    }
    this.searchForm.reset();
  }
}
